--[[

This program is free software, for more information see the LICENSE file.

Copyright Cornelius Huber
	  Alex Dorn
	  David Wieser

]]--

minetest.register_craftitem("redmattermod:redmatter_fragment", {
	description = "You get if from mining the red matter ore.",
	inventory_image = "redmattermod_redmatterfragment.png"
})

minetest.register_node("redmattermod:redmatter_ore", {
	description = "The ore.",
	tiles = {"redmattermod_redmatterore.png"},
	is_ground_content = true,
	groups = {cracky = 3},
	drop = {
		max_items = 16,
		items = {

			{
				items = {"redmattermod:redmatter_fragment 2"},
				rarity = 1
			},
			{
				items = {"redmattermod:redmatter_fragment 1"},
				rarity = 2
			},
			{
				items = {"redmattermod:redmatter_fragment 2"},
				rarity = 4
			},
			{
				items = {"redmattermod:redmatter_fragment 2"},
				rarity = 6
			},
			{
				items = {"redmattermod:redmatter_fragment 2"},
				rarity = 10
			},
			},

		}
})

--registering redmatter_ore as a ore.
minetest.register_ore({
	ore_type = "blob",
	ore = "redmattermod:redmatter_ore",
	wherein = "default:stone",
	clust_scarcity = 3 * 3 *3,
	clust_num_ores = 4,
	clust_size = 6,
	y_max = 0,
	y_min = -31000,

})


minetest.register_tool("redmattermod:redmatter_sword", {
	description = "Redmatter Sword",
	inventory_image = "redmattermod_redmattersword.png",
	tool_capabilities = {
		full_punch_interval = 0.7,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=1.70, [2]=0.70, [3]=0.10}, uses=40, maxlevel=3},
		},
		damage_groups = {fleshy=8},
	},
	sound = {breaks = "default_tool_breaks"},

})

minetest.register_craft({
	output = "redmattermod:redmatter_sword",
	recipe = {
		{"", "redmattermod:redmatter_fragment", ""},
		{"", "redmattermod:redmatter_fragment", ""},
		{"", "default:stick", ""},
	}
})

dofile(minetest.get_modpath("redmattermod").. "/bows.lua")