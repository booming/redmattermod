# Redmattermod

This is a mod for minetest. To install, please read [this guide.](https://wiki.minetest.net/Help:Installing_Mods)

Authors:
- Cornelius Huber
- Alex Dorn
- David Wieser

This mod uses code and textures from the SmallJoker/Krock [bows mod](https://github.com/SmallJoker/bows). 
Thank you for your help. Your code is awsome. 

## License
This mod is free software, you can copy it under the LPGL 3.0 License. 
